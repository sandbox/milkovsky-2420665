<?php

/**
 * @file
 * User page callback file for the email_verify_period module.
 */

/**
 * Menu callback; process one time login link and redirects to the user page on success.
 */
function _email_verify_period_confirm_account($form, &$form_state, $uid, $timestamp, $hashed_pass) {
  global $user;

  // When processing the one-time login link, we have to make sure that a
  // different user isn't already logged in.
  if ($user->uid && $user->uid != $uid) {
    // A different user is already logged in on the computer.
    $reset_link_account = user_load($uid);
    if (!empty($reset_link_account)) {
      drupal_set_message(t('You tried to use an e-mail verification link for user %resetting_user, but another user (%other_user) is already logged into the site on this computer. Please <a href="!logout">logout</a> and try using the link again.',
        array('%other_user' => $user->name, '%resetting_user' => $reset_link_account->name, '!logout' => url('user/logout'))));
    }
    else {
      // Invalid one-time link specifies an unknown user.
      drupal_set_message(t('The e-mail verification link you clicked is invalid.'));
    }
    drupal_goto();
  }
  else {
    // Time out, in seconds, until validation link expires. 24h = 86400sec.
    $timeout = variable_get('email_verify_period_link_expires', 3) * 24 * 60 * 60;
    $current = REQUEST_TIME;

    if ($timestamp <= $current && $account = user_load($uid)) {
      // Check if user is verified.
      if (in_array(variable_get('email_verify_period_verified_rid'), array_keys($account->roles))) {
        drupal_set_message(t('Great! %mail is verified.', array('%mail' => $account->mail)));
        drupal_goto();
      }
      // Time out for first time login.
      elseif ($current - $timestamp > $timeout) {
        drupal_set_message(t('You have tried to use an e-mail verification link that has already expired. Please request a new one using the form below.'));
        drupal_goto('user/email-verify-period/resend');
      }
      // Check e-mail verification hash.
      elseif ($hashed_pass == user_pass_rehash($account->pass, $timestamp, 0, $account->uid)) {
        watchdog('user', 'The user %name used this e-mail verification link at %timestamp.', array('%name' => $account->name, '%timestamp' => $timestamp));

        // Verify the user.
        email_verify_period_user_verify($account);

        // Notify the user about e-mail verification success.
        _user_mail_notify('status_activated', $account);

        // Login, update the login timestamp of the user.
        $user = $account;
        user_login_finalize();
        drupal_set_message(t('You have just verified your e-mail. Your account is now active.'));
        $redirect_after_verified = variable_get('email_verify_period_redirect_after_verified', 'user');
        drupal_goto($redirect_after_verified);
      }
      else {
        drupal_set_message(t('You have tried to use an e-mail verification link that has been used or is no longer valid. Please request a new one using the form below.'));
        drupal_goto('user/email-verify-period/resend');
      }
    }
    else {
      // Deny access, no more clues. Everything will be in the watchdog's URL
      // for the administrator to check.
      drupal_access_denied();
    }
  }
}

/**
 * Form builder; request new e-mail verification link.
 *
 * @ingroup forms
 * @see _email_verify_period_resend_validate()
 * @see _email_verify_period_resend_submit()
 */
function _email_verify_period_resend() {
  global $user;

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username or e-mail address'),
    '#size' => 60,
    '#maxlength' => max(USERNAME_MAX_LENGTH, EMAIL_MAX_LENGTH),
    '#required' => TRUE,
    '#default_value' => isset($_GET['name']) ? $_GET['name'] : '',
  );
  // Allow logged in users to request this also.
  if ($user->uid > 0) {
    $form['name']['#type'] = 'value';
    $form['name']['#value'] = $user->mail;
    $form['mail'] = array(
      '#prefix' => '<p>',
      '#markup' =>  t('The e-mail verification link will be mailed to %email.', array('%email' => $user->mail)),
      '#suffix' => '</p>',
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Send e-mail verification'));

  return $form;
}

/**
 * Validation callback; checks username, e-mail before sending verification email.
 */
function _email_verify_period_resend_validate($form, &$form_state) {
  $name = trim($form_state['values']['name']);
  // Try to load by email.
  $users = user_load_multiple(array(), array('mail' => $name, 'status' => '1'));
  $account = reset($users);
  if (!$account) {
    // No success, try to load by name.
    $users = user_load_multiple(array(), array('name' => $name, 'status' => '1'));
    $account = reset($users);
  }
  if (isset($account->uid)) {
    form_set_value(array('#parents' => array('account')), $account, $form_state);
  }
  else {
    form_set_error('name', t('Unfortunately %name is not recognized as a user name or an e-mail address.', array('%name' => $name)));
  }
}

/**
 * Submit callback; sends verification email.
 */
function _email_verify_period_resend_submit($form, &$form_state) {
  $account = $form_state['values']['account'];
  $params['account'] = $account;
  $mail = drupal_mail('email_verify_period', 'verification_resend', $account->mail, user_preferred_language($account), $params);

  if (!empty($mail)) {
    watchdog('user', 'The e-mail verification link has been mailed to %name <%email>.', array('%name' => $account->name, '%email' => $account->mail));
    drupal_set_message(t('E-mail verification link has been sent to your e-mail address.'));
  }

  $form_state['redirect'] = NULL;
}
